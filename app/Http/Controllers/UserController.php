<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class UserController extends Controller
{
    public function index(){
        
        return view('main.Adduser',compact('result'));

    }
    public function create(Request $request){
        $name=$request->input('name');
        $email=$request->input('email');
        $password=$request->input('password');
        $phone=$request->input('phone');
        $role_id=$request->input('role_id');
        $biller_id=$request->input('biller_id');
        $warehouse_id=$request->input('warehouse_id');
        $company_name=$request->input('company_name');
        $is_active=$request->input('is_active');
        $data=array('name'=>$name,'email'=>$email,'password'=>$password,'phone'=>$phone,
        'role_id'=>$role_id,'biller_id'=>$biller_id,'warehouse_id'=>$warehouse_id,'company_name'=>$company_name,'is_active'=>$is_active);
        DB::table('user')->insert($data);
        Session::flash('message');
        return redirect('Userlist');
    }
    public function view(){
        $role=DB::select('select * from role');
        $result=DB::select('select * from user');
        return view('main.Userlist',compact('result','role'));
    }

}
