<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class RoleController extends Controller
{
    public function index(){
        return view('main.Addrole');
        }
       public function create(Request $request){
            $role=$request->input('role');
            $description=$request->input('description');
            $is_active=$request->input('is_active');
            $data=array('role'=>$role,'description'=>$description,'is_active'=>$is_active);
            DB::table('role')->insert($data);
            return redirect('Rolelist');
        }
        public function view(){
            $result=DB::select('select * from role');
            return view('main.Rolelist',compact('result'));
        }

        public function show(){
            $result=DB::select('select * from role');
            return view('main.Adduser',compact('result'));
        }
}
