<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;

class TaxController extends Controller
{
    public function index(){
        return view('main.Addtax');
    }
    public function create(Request $request){
             
        $name=$request->input('name');
        $rate=$request->input('rate');
        $data=array('name'=>$name,'rate'=>$rate);
        DB::table('tax')->insert($data);
        \Session::flash('message');
        return redirect('Taxlist');
      }
    public function view(){
        $result = DB::select('select * from tax');
        return view('main.Taxlist',compact('result'));
    }
    public function show($id){
        $tax_show=DB::select('select * from tax where id=?',[$id]);

    }
Public function update(request $request){
    $name=$request->input('name');
    $rate=$request->input('rate');
    DB::update('update tax set name = ?,rate=? where id = ?',[$name,$rate,$id]);
    \Session::flash('update_message');
    return redirect('Taxlist');

}

public function destroy($id) {
    
    DB::delete('delete from tax where id = ?',[$id]);
    echo "Record deleted successfully.<br/>";
   
 }
    
}
