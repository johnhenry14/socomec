<?php

namespace App\Http\Controllers;
use DB;
use Session;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    public function index(){
        return view('main.Addunit');
    }
    public function store(Request $request){
        $code = $request->input('code');
        $name = $request->input('name');
        $data=array('code'=>$code,'name'=>$name);
         DB::table('unit')->insert($data);
         \Session::flash('message');
        return redirect('Unitlist');
        echo "Record inserted successfully.<br/>";
        echo '<a href = "/view-records">Click Here</a> to go back.';
    }

    public function view(Request $request){
       
        $result = DB::select('select * from unit');
        return view('main.Unitlist',compact('result'));
    }
    public function unit_view(Request $request){
       
        $result = DB::select('select * from unit');
        return view('main.Addproduct',compact('result'));
    }
}
