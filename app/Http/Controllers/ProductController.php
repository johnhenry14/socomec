<?php

namespace App\Http\Controllers;
use DB;
use Session;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(){
        
        return view('main.Addproduct',compact('result','unit','tax'));
    }
    public function create(Request $request){
        $name=$request->input('name');
        $code=$request->input('code');
        $type=$request->input('type');
        $barcode_symbology=$request->input('barcode_symbology');
        $category_id=$request->input('category_id');
        $unit_id=$request->input('unit_id');
        $purchase_unit_id=$request->input('purchase_unit_id');
        $cost=$request->input('cost');
        $price=$request->input('price');
        $qty=$request->input('qty');
        $alert_quantity=$request->input('alert_quantity');
        $tax_id=$request->input('tax_id');
        $tax_method=$request->input('tax_method');
        $product_details=$request->input('product_details');
        $data=array('name'=>$name,'code'=>$code,'type'=>$type,'barcode_symbology'=>$barcode_symbology,
        'category_id'=>$category_id,'unit_id'=>$unit_id,'purchase_unit_id'=>$purchase_unit_id,'cost'=>$cost,
     'price'=>$price,'qty'=>$qty,'alert_quantity'=>$alert_quantity,'tax_id'=>$tax_id,'tax_method'=>$tax_method,
    'product_details'=>$product_details);
        DB::table('products')->insert($data);
        \Session::flash('message');
        return redirect('Productlist');
    }
    public function loan(){
        
    }
    public function view(){
        $result = DB::select('select * from products');
        $category = DB::select('select * from category');
        $unit = DB::select('select * from unit');
        $tax = DB::select('select * from tax');
        return view('main.Productlist',compact('result','category','unit','tax'));
    }
}
