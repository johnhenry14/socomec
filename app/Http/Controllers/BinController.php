<?php

namespace App\Http\Controllers;
use DB;
use Session;
use Illuminate\Http\Request;
use App\Repository\Category;

class BinController extends Controller
{
    public function index()
    {
        
        return view('main.Addbin');
    }
    public function create(Request $request){
        $binname = $request->input('binname');
        $data=array('binname'=>$binname);
         DB::table('bins')->insert($data);
         \Session::flash('message');
        return redirect('ViewBins');
        echo "Record inserted successfully.<br/>";
        echo '<a href = "/view-records">Click Here</a> to go back.';
    }

    public function view(Request $request){
       
        $result = DB::select('select * from bins');
        return view('main.ViewBins',compact('result'));
    }
    public function destroy($id) {
        DB::delete('delete from bins where id = ?',[$id]);
        \Session::flash('message1', 'Successfully updated!');
        return redirect()->route('home');
       
        }
    
    
}
