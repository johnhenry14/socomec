<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    public static function insertData($data){

        $value=DB::table('category')->where('category', $data['category'])->get();
        if($value->count() == 0){
           DB::table('category')->insert($data);
        }
     }
}
