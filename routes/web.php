<?php

/*
|
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('main.Signin');
// });
Route::get('/Signup', function () {
    return view('main.Signup');
});
Route::get('/', function () {
    return view('main.Dashboard');
});

Route::get('/Fields', function () {
    return view('main.Fields');
});
Route::get('Productlist', function () {
    return view('main.Productlist');
});

Route::get('Stockcount', function () {
    return view('main.Stockcount');
});

Route::get('Addtransfer', function () {
    return view('main.Addtransfer');
});

Route::get('Transferlist', function () {
    return view('main.Transferlist');
});

Route::get('Addcategory', function () {
    return view('main.Addcategory');
});

Route::get('modal', function () {
    return view('main.modal');
});

/*  Manage Bins */
Route::resource('Addbin', 'BinController');
Route::post('create','BinController@create')-> name('bin.create');
Route::get('ViewBins','BinController@view');
Route::get('delete/{id}','BinController@destroy');

/*  Manage Categories */
Route::resource('Addcategory', 'CategoryController');
Route::post('save','CategoryController@create')-> name('category.save');
Route::get('Categorylist','CategoryController@view');
// Route::resource('Page', 'PagesController'); // localhost:8000/
Route::post('/uploadFile', 'CategoryController@uploadFile');
Route::get('delete/{id}','CategoryController@destroy');

/*  Manage Units */
Route::resource('Addunit', 'UnitController');
Route::post('store','UnitController@store')-> name('unit.store');
Route::get('Unitlist','UnitController@view');
Route::get('delete/{id}','UnitController@destroy');

/*  Manage Tax */
Route::resource('Addtax', 'TaxController');
Route::post('create_tax','TaxController@create')-> name('tax.create_tax');
Route::get('Taxlist','TaxController@view');
Route::get('delete/{id}','TaxController@destroy');

/*  Manage Product */
Route::resource('Addproduct', 'ProductController');
Route::post('product_create','ProductController@create')-> name('product.product_create');
Route::get('Productlist','ProductController@view');
Route::get('delete/{id}','ProductController@destroy');

/*  Manage Role */
Route::resource('Addrole', 'RoleController');
Route::post('role_create','RoleController@create')-> name('role.role_create');
Route::get('Rolelist','RoleController@view');
Route::get('delete/{id}','RoleController@destroy');

/*  Manage User */
Route::resource('Adduser', 'UserController');
Route::post('user_create','UserController@create')-> name('user.user_create');
Route::get('Userlist','UserController@view');
Route::get('delete/{id}','UserController@destroy');

/*  Manage Customer */
Route::resource('CustomerList', 'CustomerController');





Route::get('/Adjustment', function () {
    return view('main.Adjustment');
});

Route::get('/Users', function () {
    return view('main.Users');
});

Route::get('/Report', function () {
    return view('main.Report');
});
