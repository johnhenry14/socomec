@extends('layouts.Signin_master')
@section('title')
SOCOMEC
@endsection
@section('content')
<div class="app app-header-fixed ">
  

  <div class="container w-xxl w-auto-xs" ng-controller="SigninFormController" ng-init="app.settings.container = false;">
    
    <a href="#/" class="navbar-brand block m-t text-primary">
      <img src="{{asset('images/logo.png')}}" alt=".">
      <span class="hidden-folded m-l-xs">Socomec</span>
    </a>
    <div class="m-b-lg">
      <div class="wrapper text-center">
        <strong>Sign in to your account</strong>
      </div>
      <form name="form" class="form-validation">
        <div class="text-danger wrapper text-center" ng-show="authError">
            
        </div>
        <div class="list-group list-group-sm">
          <div class="form-group">
            <input type="email" placeholder="Email" class="form-control" ng-model="user.email" required>
          </div>
          <div class="form-group">
             <input type="password" placeholder="Password" class="form-control" ng-model="user.password" required>
          </div>
        </div>
        <button type="submit" class="btn btn-lg btn-primary btn-block" ng-click="login()" ng-disabled='form.$invalid'>Log in</button>
        <div class="text-center m-t m-b"><a ui-sref="access.forgotpwd">Forgot password?</a></div>
        <div class="line line-dashed"></div>
       
      </form>
    </div>
    
  </div>
  
  
  </div>
  

@endsection
