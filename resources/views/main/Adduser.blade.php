@extends('layouts.master')
@section('title')
SOCOMEC
@endsection
@section('content')
<!-- content -->

<div id="content" class="app-content" role="main">
        <div class="app-content-body ">
          
  
  <div class="hbox hbox-auto-xs hbox-auto-sm" ng-init="
      app.settings.asideFolded = false; 
      app.settings.asideDock = false;
    ">
    <!-- main -->
    <div class="col">
      <!-- main header -->
     
      <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Add User</h1>
      </div>
      <div class="wrapper-md" ng-controller="FormDemoCtrl">
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
          
            <div class="panel-body">
              {!! Form::open(['route'=>'user.user_create','class'=>'form']) !!}
              <div class="form-group col-lg-6">
                  {!! Form::label('UserName *') !!}
                  {!! Form::text('name',null,['class'=>'form-control']) !!}
              </div>

              
            <div class="form-group col-lg-6">
              {!! Form::label('Role *') !!}
              <select class="form-control" name="role_id" required>	
                    <option value="">--select--</option>										
                    @foreach ($result as $value)
                       <option value="{{$value->id}}">{{$value->role}}</option>
                    @endforeach
                  </select>
          </div>
          <div class="form-group col-lg-6">
            {!! Form::label('Password *') !!}
            {!! Form::text('password',null,['class'=>'form-control']) !!}
        </div>
          <div class="form-group col-lg-6">
            {!! Form::label('Biller *') !!}
            {!! Form::select('biller_id', ['1' => 'john', '2' => 'henry'],$selected = '1', ['class' => 'form-control'/*,'disabled'=>true*/ ]) !!}
        </div>
        <div class="form-group col-lg-6">
          {!! Form::label('Email *') !!}
          {!! Form::text('email',null,['class'=>'form-control']) !!}
      </div>
        <div class="form-group col-lg-6">
          {!! Form::label('warehouse') !!}
          {!! Form::select('warehouse_id', ['1' => 'Warehouse 1', '2' => 'Warehouse 2'],$selected = '1', ['class' => 'form-control'/*,'disabled'=>true*/ ]) !!}
         
      </div>
      
        <div class="form-group col-lg-6">
            {!! Form::label('Phone *') !!}
            {!! Form::text('phone',null,['class'=>'form-control']) !!}
        </div>

          <div class="form-group col-lg-6">
            {!! Form::label('Company Name') !!}
            {!! Form::text('company_name',null,['class'=>'form-control']) !!}
        </div>
      <div class="form-group">
        {!! Form::label('Active') !!}
      
        {!! Form::checkbox('is_active', 1, true) !!}
        
    </div>
              
              {!! Form::submit('Submit', ['class' => 'btn btn-primary center-block']) !!}

              {!! Form::close() !!}
              
            </div>
          </div>
        </div>
       
      </div>
      </div>

    </div>
  </div>

 
  
      </div>
    </div>

    


@endsection
