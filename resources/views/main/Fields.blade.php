@extends('layouts.master')
@section('title')
Form Fields
@endsection
@section('content')
<div class="container">
   <div class="row">
     <div class="col-lg-6">
       {!! Form::open(['class' => 'form']) !!}
       <div class="form-group">
         {!! Form::label('name', 'Text Field') !!}
         {!! Form::text('name', $value = null, ['class' => 'form-control','placeholder'=>'Text Field']) !!}
       </div>
       <div class="form-group">
            {!! Form::label('email', 'Text field (Readonly)') !!}
            {!! Form::email('email', null, ['class' => 'form-control','placeholder'=>'Text Field','readonly'=>true]) !!}
        </div>
     <div class="form-group">
         {!! Form::label('email', 'Text field (Disabled)') !!}
         {!! Form::email('email', null, ['class' => 'form-control','placeholder'=>'Text Field','disabled'=>true]) !!}
     </div>
     <div class="form-group">
      {!! Form::label('email', 'Password') !!}
      {!! Form::password('email',['class' => 'form-control']) !!}
  </div>
  <div class="form-group">
      {!! Form::label('checkbox', 'Checkbox') !!}
      {!! Form::checkbox('independent', null, true) !!}
      {!! Form::checkbox('independent', null, false) !!}
      
  </div>
  <div class="form-group">
      {!! Form::label('radios', 'Radios', ['class' => 'col-lg-2 control-label']) !!}
      {!! Form::label('radio1', 'Male') !!}
      {!! Form::radio('radio', 'Male', true, ['id' => 'radio1']) !!}
      {!! Form::label('radio2', 'Female') !!}
      {!! Form::radio('radio', 'Female', false, ['id' => 'radio2']) !!}
   </div>
  <div class="form-group">
      {!! Form::label('select', 'Select', ['class' => 'col-lg-6 control-label'] )  !!}
      {!! Form::select('OS', ['S' => 'Small', 'L' => 'Large'],$selected = 'L', ['class' => 'form-control'/*,'disabled'=>true*/ ]) !!}
   
  </div>
  <div class="form-group">
      {!! Form::label('multipleselect[]', 'Multi Select', ['class' => 'col-lg-6 control-label'] )  !!}
      {!!  Form::select('Database', ['honda' => 'Honda', 'toyota' => 'Toyota', 'subaru' => 'Subaru', 'ford' => 'Ford', 'nissan' => 'Nissan'], $selected = null, ['class' => 'form-control', 'multiple' => 'multiple']) !!}
  </div>
  <div class="form-group">
      {!! Form::label('Product Image') !!}
      {!! Form::file('image', null) !!}
  </div>
     
     <div class="form-group">
         {!! Form::label('email', 'Textarea') !!}
         {!! Form::textarea('msg', null, ['class' => 'form-control','rows'=>2,'cols'=>500]) !!}
     </div>
     
     
     {!! Form::submit('Submit', ['class' => 'btn btn-info center-block']) !!}
   
     {!! Form::close() !!}
   
     </div>
   </div>
   
     </div>
    
@endsection