@extends('layouts.master')
@section('title')
SOCOMEC
@endsection
@section('content')
@php
    $i=1;
@endphp
<!-- content -->
<div id="content" class="app-content" role="main">
  <div class="app-content-body ">
    

<div class="bg-light lter b-b wrapper-md">
<h1 class="m-n font-thin h3">Customer List</h1>
</div>
@if (Session::has('message'))
<p style="text-align: center;color: green;font-size: 18px;">Customer Added Successfully !!</p>
@endif

<div class="wrapper-md">
<div class="panel panel-default">
  <div class="panel-heading">
    <button class="btn btn-primary" id="myBtn">+ Add Customer</button></a>
  </div>
  <div class="table-responsive">
    
    <table ui-jq="dataTable" id="example" ui-options="{
        sAjaxSource: 'api/datatable.json',
        aoColumns: [
          { mData: 'engine' },
          { mData: 'browser' },
          { mData: 'platform' },
          { mData: 'version' },
          { mData: 'grade' }
        ]
      }" class="table table-striped table-bordered b-t b-b">
    
      <thead>
        <tr>
            <th>Customer Group</th>
                    <th>Name</th>
                    <th>Company Name</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Tax Number</th>
                    <th>Address</th>
                    <th>Balance</th>
                  

        </tr>
      </thead>
      <tbody>
        
          <tr>
         
              <td>
                  general
</td>
<td>dhiman</td>
<td>lioncoders</td>
<td>dhiman@gmail.com</td>
<td>+8801111111101</td>
<td></td>
<td>kajir deuri, chittagong ,bd</td>
<td>170.00</td>

            </tr>


                          
      </tbody>
    </table>
  </div>
</div>
</div>



</div>
</div>
<!-- /content -->

<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content_product">
        <span class="close">&times;</span>
        <div class="modal-header">
            <h2 class="modal-title text-center text-primary" id="exampleModalCenterTitle">Add Products</h2>
      </div>
      {!! Form::open(['route'=>'product.product_create','class'=>'form']) !!}
      <div class="form-group col-lg-6">
          {!! Form::label('Product Name *') !!}
          {!! Form::text('name',null,['class'=>'form-control']) !!}
      </div>
      <div class="form-group col-lg-6">
          {!! Form::label('Product Code *') !!}
          {!! Form::text('code',null,['class'=>'form-control']) !!}
      </div>
      <div class="form-group col-lg-6">
          {!! Form::label('Product Type *') !!}
          {!! Form::select('type', ['select' => '--select--','standard' => 'standard', 'combo' => 'combo', 'digital' => 'digital'],$selected = '', ['class' => 'form-control'/*,'disabled'=>true*/ ]) !!}
 
      </div>
      <div class="form-group col-lg-6">
          {!! Form::label('Barcode Symbology *') !!}
          {!! Form::select('barcode_symbology', ['C128' => 'CODE 128','C39' => 'CODE 39', 'UPCA' => 'UPC-A', 'UPCE' => 'UPC-E','EAN8'=>'EAN-8','EAN13'=>'EAN-13'],$selected = 'C128', ['class' => 'form-control' ]) !!}
          
      </div>
      <div class="form-group col-lg-6">
          {!! Form::label('Product Category *') !!}
         
       </div>
      
      
      <div class="form-group col-lg-6">
          {!! Form::label('Product Unit *') !!}
         
        
      </div>
      <div class="form-group col-lg-6">
          {!! Form::label('Purchase Unit') !!}
         
      </div>
      
      <div class="form-group col-lg-6">
          {!! Form::label('Product Cost *') !!}
          {!! Form::text('cost',null,['class'=>'form-control']) !!}
      </div>
      <div class="form-group col-lg-6">
          {!! Form::label('Product Price *') !!}
          {!! Form::text('price',null,['class'=>'form-control']) !!}
      </div>

      <div class="form-group col-lg-6">
          {!! Form::label('Alert Quantity') !!}
          {!! Form::text('alert_quantity',null,['class'=>'form-control']) !!}
      </div>

      <div class="form-group col-lg-6">
          {!! Form::label('Product Tax') !!}
         
      </div>

      <div class="form-group col-lg-6">
          {!! Form::label('Tax Method') !!}
          {!! Form::select('tax_method', ['1' => 'Inclusive', '2' => 'Exclusive'],$selected = '1', ['class' => 'form-control'/*,'disabled'=>true*/ ]) !!}
         
      </div>

      <div class="form-group col-lg-6">
          {!! Form::label('Product Details') !!}
          {!! Form::text('product_details',null,['class'=>'form-control']) !!}
      </div>

      {!! Form::submit('Submit', ['class' => 'btn btn-primary center-block']) !!}

      {!! Form::close() !!}
    </div>
  
  </div>
  <!-- /content -->
  <script>
      // Get the modal
      var modal = document.getElementById('myModal');
      
      // Get the button that opens the modal
      var btn = document.getElementById("myBtn");
      
      // Get the <span> element that closes the modal
      var span = document.getElementsByClassName("close")[0];
      
      // When the user clicks the button, open the modal 
      btn.onclick = function() {
        modal.style.display = "block";
      }
      
      // When the user clicks on <span> (x), close the modal
      span.onclick = function() {
        modal.style.display = "none";
      }
      
      // When the user clicks anywhere outside of the modal, close it
      window.onclick = function(event) {
        if (event.target == modal) {
          modal.style.display = "none";
        }
      }
      </script>
  


@endsection
