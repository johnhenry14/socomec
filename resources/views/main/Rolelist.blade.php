@extends('layouts.master')
@section('title')
SOCOMEC
@endsection
@section('content')
@php
$i=0;
@endphp

<!-- content -->
<div id="content" class="app-content" role="main">
  <div class="app-content-body ">
    

<div class="bg-light lter b-b wrapper-md">
<h1 class="m-n font-thin h3">User Roles</h1>
</div>

@if (Session::has('message'))
<p style="text-align: center;color: green;font-size: 18px;">Role Added Successfully !!</p>
@endif

<div class="wrapper-md">
<div class="panel panel-default">
  <div class="panel-heading">
<button class="btn btn-primary" id="myBtn"><i class="fa fa-plus" aria-hidden="true"></i> Add Role</button>
<button class="btn btn-success" id="myBtn1"><i class="fa fa-file" aria-hidden="true"></i> Import Role</button>
  </div>
  <div class="table-responsive">
    
    <table ui-jq="dataTable" id="example" ui-options="{
        sAjaxSource: 'api/datatable.json',
        aoColumns: [
          { mData: 'engine' },
          { mData: 'browser' },
          { mData: 'platform' },
          { mData: 'version' },
          { mData: 'grade' }
        ]
      }" class="table table-striped table-bordered b-t b-b">
    
      <thead>
        <tr>
          <th>S.No</th>
          <th>Role Name</th>
          <th>Description</th>
          <th>Is_Active</th>
          <th class="not-exported">Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach($result as $value)
          @php
              $i++;
          @endphp
        
               <tr>
               <td>{{$i}}</td>
               <td>{{$value->role}}</td>
               <td>{{$value->description}}</td>
               <td>{{$value->is_active}}</td>
                <td><div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action
                  <span class="caret"></span>
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu edit-options dropdown-menu-right dropdown-default" user="menu" x-placement="bottom-end" style="position: absolute; transform: translate3d(86px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                    <li>
                        <button type="button" data-id="12" class="open-EditCategoryDialog btn btn-link" data-toggle="modal" data-target="#editModal"><i class="fa fa-edit"></i> Edit</button>
                    </li>
                    <li class="divider"></li><form method="POST" action="http://localhost/application/category/12" accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="llrVdLc1ju7ItWqolMWBLWhgd2ZZdugPAUs91Pnw">
                    <li>
                      <button type="submit" class="btn btn-link" onclick="return confirmDelete()"><i class="fa fa-trash"></i> Delete</button> 
                    </li></form>
                </ul>
            </div></td>
            </tr>
            @endforeach 
           
      </tbody>
    </table>
  </div>
</div>
</div>
</div>
</div>
<!-- /content -->

<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>
        <div class="modal-header">
            <h2 class="modal-title text-center text-primary" id="exampleModalCenterTitle">Add Role</h2>
      </div>
      {!! Form::open(['route'=>'role.role_create','class'=>'form']) !!}
      <div class="form-group">
          {!! Form::label('Role') !!}
          {!! Form::text('role',null,['class'=>'form-control']) !!}
      </div>
      <div class="form-group">
        {!! Form::label('Description') !!}
        {!! Form::text('description',null,['class'=>'form-control']) !!}
    </div>
    {{ Form::hidden('is_active', '1') }}
                   
      {!! Form::submit('Submit', ['class' => 'btn btn-primary center-block']) !!}

      {!! Form::close() !!}
    </div>
  
  </div>

  <!-- The Modal -->
<div id="myModal1" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
      <span class="close1">&times;</span>
      <div class="modal-header">
          <h2 class="modal-title text-center text-primary" id="exampleModalCenterTitle">Import Role</h2>
    </div>

    <form method='post' action='/uploadFile' enctype='multipart/form-data' >
      {{ csrf_field() }}
      <div class="form-group col-lg-7">
        <label><strong>Upload CSV File *</strong></label>
        <input type='file' class="form-control" name='file' required>
      </div>
      
      <div class="form-group">
        <input type='submit' class="btn btn-primary" name='submit' value='Import' style="margin-top:25px">
    </form>
      <a href="{{asset('samplefile/sample_category.csv')}}" download class="btn btn-info" style="margin-top:25px; margin-left:10px;"><i class="fa fa-download" aria-hidden="true"></i>  Download Sample File</a>
      </div>
      
 
  
  </div>

  

</div>
  <!-- /content -->
  <script>
      // Get the modal
      var modal = document.getElementById('myModal');
      
      // Get the button that opens the modal
      var btn = document.getElementById("myBtn");
      
      // Get the <span> element that closes the modal
      var span = document.getElementsByClassName("close")[0];
      
      // When the user clicks the button, open the modal 
      btn.onclick = function() {
        modal.style.display = "block";
      }
      
      // When the user clicks on <span> (x), close the modal
      span.onclick = function() {
        modal.style.display = "none";
      }
      
      // When the user clicks anywhere outside of the modal, close it
      window.onclick = function(event) {
        if (event.target == modal) {
          modal.style.display = "none";
        }
      }

      /* Start Import Category Modal */
      var modal1 = document.getElementById('myModal1');
      var btn1 = document.getElementById("myBtn1");
      var span1 = document.getElementsByClassName("close1")[0];
      btn1.onclick = function() {
        modal1.style.display = "block";
      }
      span1.onclick = function() {
        modal1.style.display = "none";
      }
      window.onclick = function(event) {
        if (event.target == modal1) {
          modal1.style.display = "none";
        }
      }
  /* End Import Category Modal */
      </script>
@endsection
