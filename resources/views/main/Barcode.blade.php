@extends('layouts.master')
@section('title')
SOCOMEC
@endsection
@section('content')
<!-- content -->
<div id="content" class="app-content" role="main">
  <div class="app-content-body ">
    

<div class="hbox hbox-auto-xs hbox-auto-sm" ng-init="
app.settings.asideFolded = false; 
app.settings.asideDock = false;
">
<!-- main -->
<div class="col">
<!-- main header -->

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Add Product</h1>
</div>
<div class="wrapper-md" ng-controller="FormDemoCtrl">
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading font-bold">Print Barcode</div>
      <div class="panel-body">
    
        <section class="forms">
          <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            
                            <div class="card-body">
                                <p class="italic"><small>The field labels marked with * are required input fields</small></p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                              <label><strong>Add Product*</strong></label>
                                              <div class="input-group m-b">
                                                <span class="input-group-addon">
                                                  <i class="fa fa-barcode"></i>
                                                </span>
                                                <input type="text" class="form-control" placeholder="Please type product code and select...">
                                              </div>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                                <div class="table-responsive mt-3">
                                                    <table id="myTable" class="table table-hover order-list">
                                                        <thead>
                                                            <tr>
                                                                <th>Name</th>
                                                                <th>Code</th>
                                                                <th>Quantity</th>
                                                                <th><i class="fa fa-trash"></i></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group mt-2">
                                          <strong>Print: </strong>&nbsp;
                                          <strong><input type="checkbox" name="name" checked /> Product Name</strong>&nbsp;
                                          <strong><input type="checkbox" name="price" checked/> Price</strong>&nbsp;
                                          
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" value="submit" class="btn btn-primary" id="submit-button">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <div id="print-barcode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
              <div role="document" class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 id="modal_header" class="modal-title">{{trans('file.Barcode')}}</h5>&nbsp;&nbsp;
                      <button id="print-btn" type="button" class="btn btn-default btn-sm"><i class="fa fa-print"></i> {{trans('file.Print')}}</button>
                      <button type="button" id="close-btn" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                      <div id="label-content">
                      </div>
                    </div>
                  </div>
              </div>
            </div>
        </section>
        
      </div>
    </div>
  </div>
 
</div>
</div>

</div>
</div>



</div>
</div>


@endsection
