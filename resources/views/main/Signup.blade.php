@extends('layouts.Signin_master')
@section('title')
SOCOMEC
@endsection
@section('content')
<div class="app app-header-fixed ">
   <div class="container">
      <a href class="navbar-brand block m-t">Angulr</a>
      <div class="m-b-lg">
        <div class="wrapper text-center">
          <strong>Sign up to find interesting thing</strong>
        </div>

        {!! Form::open(['class'=>'form']) !!}
              <div class="form-group">
                  {!! Form::text('name',null,['class'=>'form-control','required'=>'true','placeholder'=>'UserName *']) !!}
              </div>
              
              <div class="form-group">
                  {!! Form::email('email',null,['class'=>'form-control','required'=>'true','placeholder'=>'Email *']) !!}
              </div>

              <div class="form-group">
                {!! Form::text('phone',null,['class'=>'form-control','required'=>'true']) !!}
            </div>

            <div class="form-group">
                {!! Form::text('company_name',null,['class'=>'form-control','required'=>'true']) !!}
            </div>

            <div class="form-group">
                {!! Form::text('role_id',null,['class'=>'form-control','required'=>'true']) !!}
            </div>
            
            <div class="form-group">
                {!! Form::password('password',null,['class'=>'form-control','required'=>'true']) !!}
            </div>
            
            <div class="form-group">
                {!! Form::password('password_confirmation',null,['class'=>'form-control','required'=>'true']) !!}
            </div>
            {!! Form::submit('Submit', ['class' => 'btn btn-primary center-block']) !!}

              {!! Form::close() !!}
              
       
      </div>
      
    </div>


</div>
@endsection