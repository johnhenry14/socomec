@extends('layouts.master')
@section('title')
SOCOMEC
@endsection
@section('content')
@php
$i=1;
 //echo "<pre>";print_r($result);
@endphp

<!-- content -->
<div id="content" class="app-content" role="main">
  <div class="app-content-body ">
   
<div class="bg-light lter b-b wrapper-md">
<h1 class="m-n font-thin h3">Tax List</h1>
</div>

@if (Session::has('message'))
<p style="text-align: center;color: green;font-size: 18px;">Tax Added Successfully !!</p>
@endif

@if (Session::has('update_message'))
<p style="text-align: center;color: green;font-size: 18px;">Tax Updated Successfully !!</p>
@endif

<div class="wrapper-md">
<div class="panel panel-default">
  <div class="panel-heading">
    <button class="btn btn-primary" id="myBtn">+ Add Tax</button>
  </div>
  <div class="table-responsive">
    
    <table ui-jq="dataTable" id="example" ui-options="{
        sAjaxSource: 'api/datatable.json',
        aoColumns: [
          { mData: 'engine' },
          { mData: 'browser' },
          { mData: 'platform' },
          { mData: 'version' },
          { mData: 'grade' }
        ]
      }" class="table table-striped table-bordered b-t b-b">
    
      <thead>
        <tr>
          <th>S.No</th>
          <th>Name</th>
          <th>Rate ( % )</th>
          <th class="not-exported">Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach($result as $value)
          
        
               <tr>
               <td>{{$i++}}</td>
               <td>{{$value->name}}</td>
               <td>{{$value->rate}}</td>
                <td> <a href = 'edit/{{ $value->id }}'><button class="btn btn-info"><i class="fa fa-edit"></i></button></a> 
                  <a href = 'delete/{{ $value->id }}'><button class="btn btn-danger"><i class="fa fa-trash"></i> </button></a>
                </td>
            </tr>
            @endforeach 
           
                    


                          
      </tbody>
    </table>
  </div>
</div>
</div>



</div>
</div>
<!-- /content -->

<!-- Add Tax Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>
        <div class="modal-header">
            <h2 class="modal-title text-center text-primary" id="exampleModalCenterTitle">Add Tax</h2>
      </div>
      {!! Form::open(['route'=>'tax.create_tax','class'=>'form']) !!}
      <div class="form-group">
          {!! Form::label('Tax') !!}
          {!! Form::text('name',null,['class'=>'form-control','required'=>true]) !!}
      </div>
      <div class="form-group">
          {!! Form::label('Rate( in % )') !!}
          {!! Form::text('rate',null,['class'=>'form-control','required'=>true]) !!}
      </div>
      
      
      {!! Form::submit('Submit', ['class' => 'btn btn-primary center-block']) !!}

      {!! Form::close() !!}
    </div>
  
  </div>

  
  <!-- /content -->
  <script>
      // Get the modal
      var modal = document.getElementById('myModal');
      
      // Get the button that opens the modal
      var btn = document.getElementById("myBtn");
      
      // Get the <span> element that closes the modal
      var span = document.getElementsByClassName("close")[0];
      
      // When the user clicks the button, open the modal 
      btn.onclick = function() {
        modal.style.display = "block";
      }
      
      // When the user clicks on <span> (x), close the modal
      span.onclick = function() {
        modal.style.display = "none";
      }
      
      // When the user clicks anywhere outside of the modal, close it
      window.onclick = function(event) {
        if (event.target == modal) {
          modal.style.display = "none";
        }
      }
      
      </script>


@endsection
