@extends('layouts.master')
@section('title')
SOCOMEC
@endsection
@section('content')

<!-- content -->
<div id="content" class="app-content" role="main">
        <div class="app-content-body ">
          
  
  <div class="hbox hbox-auto-xs hbox-auto-sm" ng-init="
      app.settings.asideFolded = false; 
      app.settings.asideDock = false;
    ">
    <!-- main -->
    <div class="col">
      <!-- main header -->
     
      <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Add Product</h1>
      </div>
      <div class="wrapper-md" ng-controller="FormDemoCtrl">
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">Add Product</div>
            <div class="panel-body">
              {!! Form::open(['route'=>'product.product_create','class'=>'form']) !!}
              <div class="form-group col-lg-6">
                  {!! Form::label('Product Name *') !!}
                  {!! Form::text('name',null,['class'=>'form-control']) !!}
              </div>
              <div class="form-group col-lg-6">
                  {!! Form::label('Product Code *') !!}
                  {!! Form::text('code',null,['class'=>'form-control']) !!}
              </div>
              <div class="form-group col-lg-6">
                  {!! Form::label('Product Type *') !!}
                  {!! Form::select('type', ['select' => '--select--','standard' => 'standard', 'combo' => 'combo', 'digital' => 'digital'],$selected = '', ['class' => 'form-control'/*,'disabled'=>true*/ ]) !!}
         
              </div>
              <div class="form-group col-lg-6">
                  {!! Form::label('Barcode Symbology *') !!}
                  {!! Form::select('barcode_symbology', ['C128' => 'CODE 128','C39' => 'CODE 39', 'UPCA' => 'UPC-A', 'UPCE' => 'UPC-E','EAN8'=>'EAN-8','EAN13'=>'EAN-13'],$selected = 'C128', ['class' => 'form-control' ]) !!}
                  
              </div>
              <div class="form-group col-lg-6">
                  {!! Form::label('Product Category *') !!}
                  <select class="form-control" name="category_id" required>	
                      <option value="">--select--</option>										
                      @foreach ($result as $value)   
                          <option value='{{$value->id}}'>{{$value->category}}</option>
                        @endforeach
                    </select>
               </div>
              
              
              <div class="form-group col-lg-6">
                  {!! Form::label('Product Unit *') !!}
                  <select class="form-control" name="unit_id" required>	
                      <option value="">--select--</option>										
                      @foreach ($unit as $value)   
                          <option value='{{$value->id}}'>{{$value->name}}</option>
                        @endforeach
                    </select>
                
              </div>
              <div class="form-group col-lg-6">
                  {!! Form::label('Purchase Unit') !!}
                  <select class="form-control" name="purchase_unit_id" required>	
                      <option value="">--select--</option>										
                      @foreach ($unit as $value)   
                          <option value='{{$value->id}}'>{{$value->name}}</option>
                        @endforeach
                    </select>
              </div>
              
              <div class="form-group col-lg-6">
                  {!! Form::label('Product Cost *') !!}
                  {!! Form::text('cost',null,['class'=>'form-control']) !!}
              </div>
              <div class="form-group col-lg-6">
                  {!! Form::label('Product Price *') !!}
                  {!! Form::text('price',null,['class'=>'form-control']) !!}
              </div>

              <div class="form-group col-lg-6">
                  {!! Form::label('Alert Quantity') !!}
                  {!! Form::text('alert_quantity',null,['class'=>'form-control']) !!}
              </div>

              <div class="form-group col-lg-6">
                  {!! Form::label('Product Tax') !!}
                  <select class="form-control" name="purchase_unit_id" required>	
                        <option value="">--select--</option>										
                        @foreach ($tax as $value)   
                            <option value='{{$value->id}}'>{{$value->name}}{{$value->rate}}</option>
                          @endforeach
                      </select>
              </div>

              <div class="form-group col-lg-6">
                  {!! Form::label('Tax Method') !!}
                  {!! Form::select('tax_method', ['1' => 'Inclusive', '2' => 'Exclusive'],$selected = '1', ['class' => 'form-control'/*,'disabled'=>true*/ ]) !!}
                 
              </div>

              <div class="form-group col-lg-6">
                  {!! Form::label('Product Details') !!}
                  {!! Form::textarea('product_details',null,['class'=>'form-control']) !!}
              </div>

              {!! Form::submit('Submit', ['class' => 'btn btn-primary center-block']) !!}

              {!! Form::close() !!}
              
            </div>
          </div>
        </div>
       
      </div>
      </div>

    </div>
  </div>

 
  
      </div>
    </div>


@endsection
