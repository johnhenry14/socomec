@extends('layouts.master')
@section('title')
SOCOMEC
@endsection
@section('content')
<!-- content -->
<div id="content" class="app-content" role="main">
        <div class="app-content-body ">
          
  
  <div class="hbox hbox-auto-xs hbox-auto-sm" ng-init="
      app.settings.asideFolded = false; 
      app.settings.asideDock = false;
    ">
    <!-- main -->
    <div class="col">
      <!-- main header -->
     
      <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Add Unit</h1>
      </div>
      <div class="wrapper-md" ng-controller="FormDemoCtrl">
      <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
          <div class="panel panel-default">
          
            <div class="panel-body">
              {!! Form::open(['route'=>'unit.store','class'=>'form']) !!}
              
              <div class="form-group">
                  {!! Form::label('Unit Code') !!}
                  {!! Form::text('code',null,['class'=>'form-control']) !!}
              </div>
              <div class="form-group">
                  {!! Form::label('Unit Name') !!}
                  {!! Form::text('name',null,['class'=>'form-control']) !!}
              </div>
              
              
              {!! Form::submit('Submit', ['class' => 'btn btn-primary center-block']) !!}

              {!! Form::close() !!}
              
            </div>
          </div>
        </div>
       
      </div>
      </div>

    </div>
  </div>

 
  
      </div>
    </div>


@endsection
