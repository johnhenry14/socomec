@extends('layouts.master')
@section('title')
SOCOMEC
@endsection
@section('content')
<!-- content -->
<div id="content" class="app-content" role="main">
  <div class="app-content-body ">
    

<div class="bg-light lter b-b wrapper-md">
<h1 class="m-n font-thin h3">List Category</h1>
</div>

<div class="wrapper-md">
<div class="panel panel-default">
  <div class="panel-heading">
    <button class="btn btn-primary">+ Add Category</button>
  </div>
  <div class="table-responsive">
    
    <table ui-jq="dataTable" id="example" ui-options="{
        sAjaxSource: 'api/datatable.json',
        aoColumns: [
          { mData: 'engine' },
          { mData: 'browser' },
          { mData: 'platform' },
          { mData: 'version' },
          { mData: 'grade' }
        ]
      }" class="table table-striped table-bordered b-t b-b">
    
      <thead>
        <tr>
          <th>text</th>
          <th>text</th>
          <th>text</th>
          <th>text</th>
          <th>text</th>
        </tr>
      </thead>
      <tbody>
          <tr>
              <td>Rendering engine</td>
              <td>Browser</td>
              <td>Platform(s)</td>
              <td>Engine version</td>
              <td><div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action
                  <span class="caret"></span>
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu edit-options dropdown-menu-right dropdown-default" user="menu" x-placement="bottom-end" style="position: absolute; transform: translate3d(86px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                    <li>
                        <button type="button" data-id="12" class="open-EditCategoryDialog btn btn-link" data-toggle="modal" data-target="#editModal"><i class="fa fa-edit"></i> Edit</button>
                    </li>
                    <li class="divider"></li><form method="POST" action="http://localhost/application/category/12" accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="llrVdLc1ju7ItWqolMWBLWhgd2ZZdugPAUs91Pnw">
                    <li>
                      <button type="submit" class="btn btn-link" onclick="return confirmDelete()"><i class="fa fa-trash"></i> Delete</button> 
                    </li></form>
                </ul>
            </div></td>
            </tr>
            <tr>
                <td>Rendering engine</td>
              <td>Browser</td>
              <td>Platform(s)</td>
              <td>Engine version</td>
              <td>CSS grade</td>
              </tr>
              <tr>
                  <td>Rendering engine</td>
              <td>Browser</td>
              <td>Platform(s)</td>
              <td>Engine version</td>
              <td>CSS grade</td>
                </tr>
                <tr>
                    <td>Rendering engine</td>
                    <td>Browser</td>
                    <td>Platform(s)</td>
                    <td>Engine version</td>
                    <td>CSS grade</td>
                  </tr>
                  <tr>
                      <td>Rendering engine</td>
              <td>Browser</td>
              <td>Platform(s)</td>
              <td>Engine version</td>
              <td>CSS grade</td>
                    </tr>
                    <tr>
                        <td>Rendering engine</td>
                <td>Browser</td>
                <td>Platform(s)</td>
                <td>Engine version</td>
                <td>CSS grade</td>
                      </tr>
                      <tr>
                          <td>Rendering engine</td>
                  <td>Browser</td>
                  <td>Platform(s)</td>
                  <td>Engine version</td>
                  <td>CSS grade</td>
                        </tr>
                        <tr>
                            <td>Rendering engine</td>
                    <td>Browser</td>
                    <td>Platform(s)</td>
                    <td>Engine version</td>
                    <td>CSS grade</td>
                          </tr>
                          <tr>
                              <td>Rendering engine</td>
                      <td>Browser</td>
                      <td>Platform(s)</td>
                      <td>Engine version</td>
                      <td>CSS grade</td>
                            </tr>
                            <tr>
                                <td>Rendering engine</td>
                        <td>Browser</td>
                        <td>Platform(s)</td>
                        <td>Engine version</td>
                        <td>CSS grade</td>
                              </tr>
                              <tr>
                                  <td>john</td>
                          <td>sharmila</td>
                          <td>Priya</td>
                          <td>vivek</td>
                          <td>indhu</td>
                                </tr>
                    


                          
      </tbody>
    </table>
  </div>
</div>
</div>



</div>
</div>
<!-- /content -->


@endsection
