@extends('layouts.master')
@section('title')
SOCOMEC
@endsection
@section('content')
<!-- content -->
<div id="content" class="app-content" role="main">
        <div class="app-content-body ">
          
  
  <div class="hbox hbox-auto-xs hbox-auto-sm" ng-init="
      app.settings.asideFolded = false; 
      app.settings.asideDock = false;
    ">
    <!-- main -->
    <div class="col">
      <!-- main header -->
      <div class="bg-light lter b-b wrapper-md">
        <div class="row">
          <div class="col-lg-12 col-sm-6 col-xs-12">
            <h1 class="m-n font-thin h3 text-black">Adjustment</h1>
            <small class="text-muted">Welcome to SOCOMEC Warehouse Management</small>
          </div>
         
        </div>
      </div>

      <section class="forms">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-md-12">
                      <div class="card">
                         
                          <div class="card-body">
                              
                              <form method="POST" action="http://localhost/application/qty_adjustment" accept-charset="UTF-8" id="adjustment-form" enctype="multipart/form-data"><input name="_token" type="hidden" value="rw0iLi8p7OiCrjvtiIgHBPqyI9H0NhKdorhsQETu">
                              <div class="row">
                                  <div class="col-md-12">
                                      <div class="row">
                                          <div class="col-md-4">
                                              <div class="form-group">
                                                  <label><strong>Warehouse *</strong></label>
                                                  <div class="btn-group bootstrap-select form-control"><button type="button" class="btn dropdown-toggle bs-placeholder btn-link" data-toggle="dropdown" role="button" data-id="warehouse_id" title="Select warehouse..."><span class="filter-option pull-left">Select warehouse...</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox"><div class="bs-searchbox"><input type="text" class="form-control" autocomplete="off" role="textbox" aria-label="Search"></div><div class="dropdown-menu inner" role="listbox" aria-expanded="false"><a tabindex="0" class="dropdown-item" data-original-index="1"><span class="dropdown-item-inner " data-tokens="null" role="option" tabindex="0" aria-disabled="false" aria-selected="false"><span class="text">warehouse 1</span><span class="fa fa-check check-mark"></span></span></a><a tabindex="0" class="dropdown-item" data-original-index="2"><span class="dropdown-item-inner " data-tokens="null" role="option" tabindex="0" aria-disabled="false" aria-selected="false"><span class="text">warehouse 2</span><span class="fa fa-check check-mark"></span></span></a></div></div><select required="" id="warehouse_id" name="warehouse_id" class="selectpicker form-control" data-live-search="true" data-live-search-style="begins" title="Select warehouse..." tabindex="-98"><option class="bs-title-option" value="">Select warehouse...</option>
                                                                                                      <option value="1">warehouse 1</option>
                                                                                                      <option value="2">warehouse 2</option>
                                                                                                  </select></div>
                                              </div>
                                          </div>
                                          <div class="col-md-4">
                                              <div class="form-group">
                                                  <label><strong>Attach Document</strong></label>
                                                  <input type="file" name="document" class="form-control">
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-lg-8">
                                            <label><strong>Select Product</strong></label>
                                      <div class="input-group m-b">
                                            
                                        <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                        
                                        <input type="text" class="form-control" placeholder="Username">
                                      </div>
                                    </div>
                                      <div class="row mt-5">
                                          <div class="col-md-12">
                                              <h5>Order Table *</h5>
                                              <div class="table-responsive mt-3">
                                                  <table id="myTable" class="table table-hover order-list">
                                                      <thead>
                                                          <tr>
                                                              <th>Name</th>
                                                              <th>Code</th>
                                                              <th>Quantity</th>
                                                              <th>Action</th>
                                                              <th><i class="fa fa-trash "></i></th>
                                                          </tr>
                                                      </thead>
                                                      <tbody>
                                                      </tbody>
                                                      <tfoot class="tfoot active">
                                                          <tr><th colspan="2">Total</th>
                                                          <th id="total-qty" colspan="2">0</th>
                                                          <th><i class="fa fa-trash"></i></th>
                                                      </tr></tfoot>
                                                  </table>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="col-md-2">
                                              <div class="form-group">
                                                  <input type="hidden" name="total_qty">
                                                  <input type="hidden" name="item">
                                              </div>
                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="col-md-12">
                                              <div class="form-group">
                                                  <label><strong>Note</strong></label>
                                                  <textarea rows="5" class="form-control" name="note"></textarea>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <input type="submit" value="Submit" class="btn btn-primary" id="submit-button">
                                      </div>
                                  </div>
                              </div>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>   
      </section>
  
     
    </div>
    
   
   
  </div>

 
  
      </div>
    </div>


@endsection
