@extends('layouts.master')
@section('title')
SOCOMEC
@endsection
@section('content')
@php
    $i=1;
@endphp
<!-- content -->
<div id="content" class="app-content" role="main">
  <div class="app-content-body ">
    

<div class="bg-light lter b-b wrapper-md">
<h1 class="m-n font-thin h3">Product List</h1>
</div>
@if (Session::has('message'))
<p style="text-align: center;color: green;font-size: 18px;">Product Added Successfully !!</p>
@endif

<div class="wrapper-md">
<div class="panel panel-default">
  <div class="panel-heading">
    <button class="btn btn-primary" id="myBtn"><i class="fa fa-plus"></i>  Add Product</button>
    <button class="btn btn-success" id="myBtn1"><i class="fa fa-file"></i>  import Product</button>
  </div>
  <div class="table-responsive">
    
    <table ui-jq="dataTable" id="example" ui-options="{
        sAjaxSource: 'api/datatable.json',
        aoColumns: [
          { mData: 'engine' },
          { mData: 'browser' },
          { mData: 'platform' },
          { mData: 'version' },
          { mData: 'grade' }
        ]
      }" class="table table-striped table-bordered b-t b-b">
    
      <thead>
        <tr>
            <th>S.No</th>
            <th>Product Name</th>
            <th>Product Code</th>
            <th>Barcode</th>
            <th>Quantity</th>
            {{-- <th>Category</th>
            <th>cost</th>
            <th>Unit</th>
            <th>Price</th> --}}
            <th class="not-exported">Action</th>
        </tr>
      </thead>
      <tbody>
          @foreach ($result as $value)
          <tr>
         
              <td>{{$i++}}</td>
            <td>{{$value->name}}</td>
              <td>{{$value->code}}</td>
              <td>-</td>
              <td>{{$value->alert_quantity}}</td>
              {{-- <td>{{$value->category_id}}</td>
              <td>{{$value->cost}}</td>
              <td>{{$value->unit_id}}</td>
              <td>{{$value->price}}</td> --}}
             
             
                  <td><div class="btn-group">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                          </button>
                          <ul class="dropdown-menu edit-options dropdown-menu-right dropdown-default" user="menu">
                              <li>
                                  <a href="" class="btn btn-link"><i class="fa fa-credit-card" aria-hidden="true"></i> Loan</a>
                              </li>
                                  <li>
                                      <a href="" class="btn btn-link"><i class="fa fa-money" aria-hidden="true"></i> Sale</a>
                                      </li>
                              <li>
                              <a href="" class="btn btn-link"><i class="fa fa-edit"></i> Edit</a>
                              </li>
                             
                              <li>
                                  <a href="" class="btn btn-link"><i class="fa fa-trash"></i> Delete</a>
                              </li>
                              
                                                          </ul>
                      </div>
                  </td>
            </tr>
                    
            @endforeach

                          
      </tbody>
    </table>
  </div>
</div>
</div>



</div>
</div>
<!-- /content -->

<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content_product">
        <span class="close">&times;</span>
        <div class="modal-header">
            <h2 class="modal-title text-center text-primary" id="exampleModalCenterTitle">Add Products</h2>
      </div>
      {!! Form::open(['route'=>'product.product_create','class'=>'form']) !!}
      <div class="form-group col-lg-6">
          {!! Form::label('Product Name *') !!}
          {!! Form::text('name',null,['class'=>'form-control']) !!}
      </div>
      <div class="form-group col-lg-6">
          {!! Form::label('Product Code *') !!}
          {!! Form::text('code',null,['class'=>'form-control']) !!}
      </div>
      <div class="form-group col-lg-6">
          {!! Form::label('Product Type *') !!}
          {!! Form::select('type', ['select' => '--select--','standard' => 'standard', 'combo' => 'combo', 'digital' => 'digital'],$selected = '', ['class' => 'form-control'/*,'disabled'=>true*/ ]) !!}
 
      </div>
      <div class="form-group col-lg-6">
          {!! Form::label('Barcode Symbology *') !!}
          {!! Form::select('barcode_symbology', ['C128' => 'CODE 128','C39' => 'CODE 39', 'UPCA' => 'UPC-A', 'UPCE' => 'UPC-E','EAN8'=>'EAN-8','EAN13'=>'EAN-13'],$selected = 'C128', ['class' => 'form-control' ]) !!}
          
      </div>
      <div class="form-group col-lg-6">
          {!! Form::label('Product Category *') !!}
          <select class="form-control" name="category_id" required>	
              <option value="">--select--</option>										
              @foreach ($category as $value)   
                  <option value='{{$value->id}}'>{{$value->category}}</option>
                @endforeach
            </select>
       </div>
      
      
      <div class="form-group col-lg-6">
          {!! Form::label('Product Unit *') !!}
          <select class="form-control" name="unit_id" required>	
              <option value="">--select--</option>										
              @foreach ($unit as $value)   
                  <option value='{{$value->id}}'>{{$value->name}}</option>
                @endforeach
            </select>
        
      </div>
      <div class="form-group col-lg-6">
          {!! Form::label('Purchase Unit') !!}
          <select class="form-control" name="purchase_unit_id" required>	
              <option value="">--select--</option>										
              @foreach ($unit as $value)   
                  <option value='{{$value->id}}'>{{$value->name}}</option>
                @endforeach
            </select>
      </div>
      
      <div class="form-group col-lg-6">
          {!! Form::label('Product Cost *') !!}
          {!! Form::text('cost',null,['class'=>'form-control']) !!}
      </div>
      <div class="form-group col-lg-6">
          {!! Form::label('Product Price *') !!}
          {!! Form::text('price',null,['class'=>'form-control']) !!}
      </div>

      <div class="form-group col-lg-6">
          {!! Form::label('Alert Quantity') !!}
          {!! Form::text('alert_quantity',null,['class'=>'form-control']) !!}
      </div>

      <div class="form-group col-lg-6">
          {!! Form::label('Product Tax') !!}
          <select class="form-control" name="purchase_unit_id" required>	
                <option value="">--select--</option>										
                @foreach ($tax as $value)   
                    <option value='{{$value->id}}'>{{$value->name}}{{$value->rate}}</option>
                  @endforeach
              </select>
      </div>

      <div class="form-group col-lg-6">
          {!! Form::label('Tax Method') !!}
          {!! Form::select('tax_method', ['1' => 'Inclusive', '2' => 'Exclusive'],$selected = '1', ['class' => 'form-control'/*,'disabled'=>true*/ ]) !!}
         
      </div>

      <div class="form-group col-lg-6">
          {!! Form::label('Product Details') !!}
          {!! Form::text('product_details',null,['class'=>'form-control']) !!}
      </div>

      {!! Form::submit('Submit', ['class' => 'btn btn-primary center-block']) !!}

      {!! Form::close() !!}
    </div>
  
  </div>


  <!-- The Modal -->
<div id="myModal1" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
      <span class="close1">&times;</span>
      <div class="modal-header">
          <h2 class="modal-title text-center text-primary" id="exampleModalCenterTitle">Import Product</h2>
    </div>

    <form method='post' action='/uploadFile' enctype='multipart/form-data' >
      {{ csrf_field() }}
      <div class="form-group col-lg-7">
        <label><strong>Upload CSV File *</strong></label>
        <input type='file' class="form-control" name='file' required>
      </div>
      
      <div class="form-group">
        <input type='submit' class="btn btn-primary" name='submit' value='Import' style="margin-top:25px">
    </form>
      <a href="{{asset('samplefile/sample_category.csv')}}" download class="btn btn-info" style="margin-top:25px; margin-left:10px;"><i class="fa fa-download" aria-hidden="true"></i>  Download Sample File</a>
      </div>
      
 
  
  </div>

  

</div>
  <!-- /content -->
  <script>
      // Get the modal
      var modal = document.getElementById('myModal');
      
      // Get the button that opens the modal
      var btn = document.getElementById("myBtn");
      
      // Get the <span> element that closes the modal
      var span = document.getElementsByClassName("close")[0];
      
      // When the user clicks the button, open the modal 
      btn.onclick = function() {
        modal.style.display = "block";
      }
      
      // When the user clicks on <span> (x), close the modal
      span.onclick = function() {
        modal.style.display = "none";
      }
      
      // When the user clicks anywhere outside of the modal, close it
      window.onclick = function(event) {
        if (event.target == modal) {
          modal.style.display = "none";
        }
      }

       /* Start Import Category Modal */
       var modal1 = document.getElementById('myModal1');
      var btn1 = document.getElementById("myBtn1");
      var span1 = document.getElementsByClassName("close1")[0];
      btn1.onclick = function() {
        modal1.style.display = "block";
      }
      span1.onclick = function() {
        modal1.style.display = "none";
      }
      window.onclick = function(event) {
        if (event.target == modal1) {
          modal1.style.display = "none";
        }
      }
  /* End Import Category Modal */
      </script>
  


@endsection
