@extends('layouts.master')
@section('title')
SOCOMEC
@endsection
@section('content')
<!-- content -->
<div id="content" class="app-content" role="main">
        <div class="app-content-body ">
          
  
  <div class="hbox hbox-auto-xs hbox-auto-sm" ng-init="
      app.settings.asideFolded = false; 
      app.settings.asideDock = false;
    ">
    <!-- main -->
    <div class="col">
      <!-- main header -->
     
      <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Add Role</h1>
      </div>
      <div class="wrapper-md" ng-controller="FormDemoCtrl">
      <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
          <div class="panel panel-default">
          
            <div class="panel-body">
              {!! Form::open(['route'=>'role.role_create','class'=>'form']) !!}
              <div class="form-group">
                  {!! Form::label('Role') !!}
                  {!! Form::text('role',null,['class'=>'form-control']) !!}
              </div>
              <div class="form-group">
                {!! Form::label('Description') !!}
                {!! Form::text('description',null,['class'=>'form-control']) !!}
            </div>
            {{ Form::hidden('is_active', '1') }}
                           
              {!! Form::submit('Submit', ['class' => 'btn btn-primary center-block']) !!}

              {!! Form::close() !!}
              
            </div>
          </div>
        </div>
       
      </div>
      </div>

    </div>
  </div>

 
  
      </div>
    </div>


@endsection
